package main;

public class Main {
	public static void main(String[] args) {
		String text1 = new String("John \"JJ\" Snow")	;
		String text2 = new String ("Use \\, not /");
		String text3 = new String("name\tdmg");
		String text4 = new String("First line\nSecond Line");
		
		System.out.println(text1);
		System.out.println(text2);
		System.out.println(text3);
		System.out.println(text4);
		
		String text5 = new String("Hey there");
		String text6 = new String(", who are you?");
		System.out.print(text5);
		System.out.println(text6);
	}
}
